/**
 * Funcion que crea 5 divs
 * @return {[string]} [html con 5 divs]
 */
function ej1(){
	let divs="";
	for(var i = 0; i < 5; i++){
		divs+="<div>"+(i+1)+"</div>";
		}
	console.log("llega a ej1 1.js");
	return divs;
}
/**
 * Se le pasa un json y muestra el contenido en divs
 * @return {[string]} [retorna codigo html]
 */
function ej2(elJson){
	let divs="";
	for(let i in elJson){
		console.log("la clave es "+i+" y el valor es "+elJson[i]);
		divs+="<div>la clave es "+i+" y el valor es "+elJson[i]+"</div>";
	}
	return divs;
}